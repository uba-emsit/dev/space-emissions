import pytest
from shapely.geometry import shape

from eocalc.methods.base import DateRange


@pytest.fixture
def period_long():
    return DateRange("2018-10-15", "2020-02-29")


@pytest.fixture
def period_medium():
    return DateRange("2019-01-01", "2020-01-31")


@pytest.fixture
def period_medium_other():
    return DateRange("2019-11-01", "2020-12-31")


@pytest.fixture
def period_medium_third():
    return DateRange("2020-01-01", "2020-11-15")


@pytest.fixture
def period_short():
    return DateRange("2020-02-01", "2020-02-29")


@pytest.fixture
def period_short_other():
    return DateRange("2020-02-01", "2020-02-20")


@pytest.fixture
def period_very_short():
    return DateRange("2018-10-15", "2018-10-25")


@pytest.fixture
def region_sample_south():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[-110., -20.], [140., -20.], [180., -40.], [-180., -30.], [-110., -20.]]]]})


@pytest.fixture
def region_sample_north():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[-110., 20.], [140., 20.], [180., 40.], [-180., 30.], [-110., 20.]]]]})


@pytest.fixture
def region_sample_span_equator():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[-110., 20.], [140., -20.], [180., -40.], [-180., -30.], [-110., 20.]]]]})
