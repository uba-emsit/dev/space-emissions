from datetime import date

import numpy
import pytest
from pandas import Series
from shapely.geometry import MultiPolygon, shape

from eocalc.context import Pollutant, GNFR
from eocalc.methods.base import DateRange, EOEmissionCalculator, TimeAggregation


@pytest.fixture
def year_2019_from_strs():
    return DateRange("2019-01-01", "2019-12-31")


@pytest.fixture
def year_2019_from_dates():
    return DateRange(date.fromisoformat("2019-01-01"), date.fromisoformat("2019-12-31"))


@pytest.fixture
def year_2020():
    return DateRange("2020-01-01", "2020-12-31")


@pytest.fixture
def year_2020_other():
    return DateRange("2020-01-01", "2020-12-31")


@pytest.fixture
def august_2018():
    return DateRange("2018-08-01", "2018-08-31")


@pytest.fixture
def one_day():
    return DateRange("2018-08-01", "2018-08-01")


@pytest.fixture
def future_short():
    return DateRange("2030-01-15", "2030-04-30")


@pytest.fixture
def future_long():
    return DateRange("2030-01-15", "2042-12-31")


class TestDataRange:

    def test_equals(self, year_2019_from_strs, year_2019_from_dates, year_2020, year_2020_other):
        assert year_2019_from_strs == DateRange(end="2019-12-31", start="2019-01-01")
        assert year_2019_from_dates == DateRange(end="2019-12-31", start="2019-01-01")
        assert year_2020 != DateRange(end="2019-12-31", start="2019-01-01")
        assert year_2020 != year_2019_from_dates
        assert year_2020 != year_2019_from_strs
        assert year_2020 == year_2020_other

    def test_period_length(self, year_2019_from_dates, year_2020, august_2018, one_day):
        assert 365 == len(year_2019_from_dates)
        assert 366 == len(year_2020)
        assert 31 == len(august_2018)
        assert 1 == len(one_day)

    def test_days(self, year_2019_from_dates, year_2020, august_2018, one_day, future_short, future_long):
        assert year_2019_from_dates.days == len(year_2019_from_dates)
        assert year_2020.days == len(year_2020)
        assert august_2018.days == len(august_2018)
        assert one_day.days == len(one_day)
        assert future_short.days == len(future_short)
        assert future_long.days == len(future_long)

    def test_months(self, year_2019_from_dates, year_2020, august_2018, one_day, future_short, future_long):
        assert year_2019_from_dates.months == 12
        assert year_2020.months == 12
        assert august_2018.months == 1
        assert one_day.months == 1
        assert future_short.months == 4
        assert future_long.months == 13 * 12

    def test_years(self, year_2019_from_dates, year_2020, august_2018, one_day, future_short, future_long):
        assert year_2019_from_dates.years == 1
        assert year_2020.years == 1
        assert august_2018.years == 1
        assert one_day.years == 1
        assert future_short.years == 1
        assert future_long.years == 13

    def test_to_string(self, year_2019_from_strs):
        assert str(year_2019_from_strs) == "[2019-01-01 to 2019-12-31, 365 days]"

    def test_hash(self, year_2019_from_dates, year_2020, year_2020_other):
        assert hash(year_2019_from_dates) != hash(year_2020)
        assert hash(year_2020) == hash(year_2020_other)

    def test_iter(self, august_2018):
        count = 0
        for _ in august_2018:
            count += 1

        assert 31 == count

    def test_invalid_init_input(self):
        with pytest.raises(TypeError):
            DateRange()
        with pytest.raises(TypeError):
            DateRange(1, "")
        with pytest.raises(ValueError):
            DateRange(end="alice", start="bob")

    def test_bad_period(self):
        with pytest.raises(ValueError):
            DateRange(start="2019-01-01", end="2018-12-31")
        with pytest.raises(ValueError):
            DateRange(start="2019-01-01", end="2019-12-31").end = "2018-12-31"
        with pytest.raises(ValueError):
            DateRange(start="2019-01-01", end="2019-12-31").start = "2020-12-31"


@pytest.fixture
def calc():
    class MyEOEmissionCalculator(EOEmissionCalculator):

        @staticmethod
        def minimum_area_size() -> int:
            return 42

        @staticmethod
        def coverage() -> MultiPolygon:
            return shape({"type": "MultiPolygon",
                          "coordinates": [[[[-180., -90.], [180., -90.], [180., 0.], [-180., 0.], [-180., -90.]]]]})

        @staticmethod
        def minimum_period_length() -> int:
            return 42

        @staticmethod
        def earliest_start_date() -> date:
            return date.fromisoformat("1000-01-01")

        @staticmethod
        def latest_end_date() -> date:
            return date.fromisoformat("2999-12-31")

        @staticmethod
        def minimum_granularity() -> TimeAggregation:
            return TimeAggregation.MONTHLY

        @staticmethod
        def supports(pollutant: Pollutant) -> bool:
            return pollutant == Pollutant.NH3

        def run(self, region=None, period=None, granularity=None, pollutant=None) -> dict:
            return 42

    return MyEOEmissionCalculator()


@pytest.fixture
def region_clone_coverage():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[-180., -90.], [180., -90.], [180., 0.], [-180., 0.], [-180., -90.]]]]})


@pytest.fixture
def region_other_covered():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[0., 0.], [0., -1.], [-1., -1.], [-1., 0.], [0., 0.]]]]})


@pytest.fixture
def region_other_not_covered():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[0., 0.], [0., 1.], [1., 1.], [1., 0.], [0., 0.]]]]})


@pytest.fixture
def region_too_small_but_covered():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[0., 0.], [0., -.001], [-.001, -.001], [-.001, 0.], [0., 0.]]]]})


@pytest.fixture
def region_box_north_of_equator():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[0., 0.], [0., 1.], [1., 1.], [1., 0.], [0., 0.]]]]})


@pytest.fixture
def region_box_span_equator():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[-0.5, -0.5], [-0.5, 0.5], [0.5, 0.5], [0.5, -0.5], [-0.5, -0.5]]]]})


@pytest.fixture
def region_far_out_at_the_date_line():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[-202, -90], [-202, 22], [301, 22], [301, -90], [-202, -90]]]]})


@pytest.fixture
def region_small_but_well_known():
    return shape({"type": "MultiPolygon",
                  "coordinates": [[[[-122.255, 37.188], [-122.255, 37.438], [-122.38, 37.438]]]]})


@pytest.fixture
def period_supported():
    return DateRange("2019-01-01", "2019-12-31")


@pytest.fixture
def period_not_supported():
    return DateRange("0001-01-01", "2019-12-31")


@pytest.fixture
def period_not_supported_other():
    return DateRange("2000-01-01", "9999-12-31")


@pytest.fixture
def period_too_short():
    return DateRange("2100-01-01", "2100-01-31")


@pytest.fixture
def granularity_supported():
    return TimeAggregation.YEARLY


@pytest.fixture
def granularity_too_fine():
    return TimeAggregation.DAILY


@pytest.fixture
def pollutant_supported():
    return Pollutant.NH3


@pytest.fixture
def pollutant_not_supported():
    return Pollutant.NOx


class TestCalcMethods:

    def test_covers(self, calc, region_sample_south, region_sample_north, region_sample_span_equator,
                    region_clone_coverage, region_other_covered, region_other_not_covered,
                    region_too_small_but_covered):
        assert not calc.covers(region_sample_north)
        assert not calc.covers(region_sample_span_equator)
        assert not calc.covers(region_other_not_covered)
        assert calc.covers(region_sample_south)
        assert calc.covers(region_clone_coverage)
        assert calc.covers(region_other_covered)
        assert calc.covers(region_too_small_but_covered)

    def test_validate_no_problem(self, calc, region_other_covered, period_supported,
                                 granularity_supported, pollutant_supported):
        calc._validate(region_other_covered, period_supported, granularity_supported, pollutant_supported)

    def test_validate_bad_region(self, calc, region_other_not_covered, region_too_small_but_covered,
                                 period_supported, granularity_supported, pollutant_supported):
        with pytest.raises(ValueError):
            calc._validate(region_other_not_covered, period_supported, granularity_supported, pollutant_supported)
        with pytest.raises(ValueError):
            calc._validate(region_too_small_but_covered, period_supported, granularity_supported, pollutant_supported)

    def test_validate_bad_period(self, calc, region_other_covered, period_not_supported, period_not_supported_other,
                                 period_too_short, granularity_supported, pollutant_supported):
        with pytest.raises(ValueError):
            calc._validate(region_other_covered, period_not_supported, granularity_supported, pollutant_supported)
        with pytest.raises(ValueError):
            calc._validate(region_other_covered, period_not_supported_other, granularity_supported, pollutant_supported)
        with pytest.raises(ValueError):
            calc._validate(region_other_covered, period_too_short, granularity_supported, pollutant_supported)

    def test_validate_granularity_too_fine(self, calc, region_other_covered, period_supported,
                                           granularity_too_fine, pollutant_supported):
        with pytest.raises(ValueError):
            calc._validate(region_other_covered, period_supported, granularity_too_fine, pollutant_supported)

    def test_validate_bad_pollutants(self, calc, region_other_covered, period_supported,
                                     granularity_supported, pollutant_not_supported):
        with pytest.raises(ValueError):
            calc._validate(region_other_covered, period_supported, granularity_supported, pollutant_not_supported)

    def test_validate_all_wrong(self, calc, region_other_not_covered, period_not_supported,
                                granularity_too_fine, pollutant_not_supported):
        with pytest.raises(ValueError):
            calc._validate(region_other_not_covered, period_not_supported, granularity_too_fine, pollutant_not_supported)

    def test_create_gnfr_frame(self, calc):
        for pollutant in Pollutant:
            frame = calc._create_gnfr_table(pollutant)
            assert len(GNFR) + 1 == len(frame)
            assert 3 == len(frame.columns)
            assert "A_PublicPower" == frame.iloc[0].name.name
            assert "Totals" == frame.iloc[-1].name
            assert frame.iloc[:, 0].name.startswith(pollutant.name)
            assert frame.iloc[:, 1].name.startswith("Umin")
            assert frame.iloc[:, 2].name.startswith("Umax")

    @pytest.mark.parametrize(
        "values, uncertainties, result", [
            (Series(10), Series(2), 2),
            (Series([42, 15]), Series([0, 0]), 0),
            (Series([0, 0]), Series([15, 42]), 0),
            (Series([10, 10]), Series([2, 4]), ((10 * 2) ** 2 + (10 * 4) ** 2) ** 0.5 / 20),
            (Series([10, -10]), Series([-2, 4]), ((10 * 2) ** 2 + (10 * 4) ** 2) ** 0.5 / 20),
            (Series([-10, -5]), Series([-2, -4]), ((10 * 2) ** 2 + (5 * 4) ** 2) ** 0.5 / 15),
            (Series([-10, -5]), Series(3 for _ in range(2)), ((10 * 3) ** 2 + (5 * 3) ** 2) ** 0.5 / 15)
        ])
    def test_combine_uncertainties_simple(self, calc, values, uncertainties, result):
        assert result == calc._combine_uncertainties(values, uncertainties)

    @pytest.mark.parametrize(
        "values, uncertainties, result", [
            (Series([10], index=['A']), Series([2], index=['A']), 2),
            (Series([10], index=['A']), Series([2], index=['B']), 2),
            (Series([10], index=['B']), Series([2], index=['A']), 2),
            (Series([10]), Series([2], index=['Y']), 2),
            (Series([10], index=['X']), Series([2]), 2)
        ])
    def test_combine_uncertainties_drop_index(self, calc, values, uncertainties, result):
        assert result == calc._combine_uncertainties(values, uncertainties)

    @pytest.mark.parametrize(
        "values, uncertainties, result", [
            (Series([10, numpy.nan, 10]), Series([2, 3, 4]), ((10 * 2) ** 2 + (10 * 4) ** 2) ** 0.5 / 20),
            (Series([10, numpy.nan, numpy.nan]), Series([2, 3, 4]), ((10 * 2) ** 2) ** 0.5 / 10),
            (Series([numpy.nan, numpy.nan, numpy.nan]), Series([2, 3, 4]), 0)
        ])
    def test_combine_uncertainties_partly_nan(self, calc, values, uncertainties, result):
        assert result == calc._combine_uncertainties(values, uncertainties)

    @pytest.mark.parametrize(
        "values, uncertainties, result", [
            (Series([]), Series([]), 0),
            (Series([]), Series([2]), 0)
        ])
    def test_combine_uncertainties_partly_empty(self, calc, values, uncertainties, result):
        assert result == calc._combine_uncertainties(values, uncertainties)

    def test_combine_uncertainties_series_lengths_do_not_match(self, calc):
        assert ((10 * 2) ** 2 + (20 * 3) ** 2) ** 0.5 / 30 == \
               calc._combine_uncertainties(Series([10, 20]), Series([2, 3, 4]))
        with pytest.raises(ValueError):
            calc._combine_uncertainties(Series([10, 20]), Series([2]))

    def test_combine_uncertainties_bad_uncertainties(self, calc):
        with pytest.raises(ValueError):
            calc._combine_uncertainties(Series([10, 20]), Series([2, numpy.nan]))

    @pytest.mark.parametrize(
        "width, height, snap, cell_count, x, y", [
            (10, 10, False, 1, 1, 1),
            (2, 2, False, 1, 1, 1),
            (1, 1, False, 1, 1, 1),
            (1, 1, True, 1, 1, 1),
            (0.5, 1, False, 2, 2, 1),
            (1, 0.5, True, 2, 1, 2),
            (0.5, 0.5, False, 4, 2, 2),
            (0.5, 0.5, True, 4, 2, 2),
            (0.3, 0.3, False, 16, 4, 4),
            (0.3, 0.3, True, 16, 4, 4)
        ])
    def test_create_grid_simple(self, calc, width, height, snap, region_box_north_of_equator, cell_count, x, y):
        grid, dimensions = calc._create_grid(region_box_north_of_equator, width, height, snap=snap)
        assert cell_count == len(grid)
        assert dimensions == (x, y)

    @pytest.mark.parametrize(
        "width, height, padding, snap, cell_count, x, y", [
            (0.5, 1, (.5, .5), False, 8, 4, 2),
            (1, 0.5, (.5, .5), True, 12, 3, 4),
            (0.5, 0.5, (3., 1.), False, 84, 14, 6),
            (0.5, 0.5, (1., 3.), True, 84, 6, 14),
            (0.3, 0.3, (.08, .08), False, 16, 4, 4),
            (0.3, 0.3, (.08, .08), True, 25, 5, 5),
            (0.3, 0.3, (.1, .1), False, 16, 4, 4),
            (0.3, 0.3, (.1, .1), True, 25, 5, 5)
        ])
    def test_create_grid_with_padding(self, calc, width, height, padding, snap, region_box_north_of_equator, cell_count, x, y):
        grid, dimensions = calc._create_grid(region_box_north_of_equator, width, height, padding=padding, snap=snap)
        assert cell_count == len(grid)
        assert dimensions == (x, y)

    @pytest.mark.parametrize(
        "width, height, snap, cell_count", [
            (10, 10, False, 1),
            (10, 10, True, 4),
            (2, 2, False, 1),
            (1, 1, False, 1),
            (1, 1, True, 4),
            (0.5, 1, False, 2),
            (1, 0.5, True, 4),
            (0.5, 0.5, False, 4),
            (0.5, 0.5, True, 4),
            (0.3, 0.3, True, 16),
            (0.3, 0.3, False, 16)
        ])
    def test_create_grid_span_equator(self, calc, width, height, snap, region_box_span_equator, cell_count):
        assert cell_count == len(calc._create_grid(region_box_span_equator, width, height, snap=snap)[0])

    @pytest.mark.parametrize(
        "width, height, snap, cell_count", [
            (10, 10, False, 51 * 12),
            (10, 10, True, 52 * 12),
            (50, 10, False, 11 * 12),
            (50, 10, True, 12 * 12)
        ])
    def test_create_beyond_minus_180_degrees(self, calc, width, height, snap, region_far_out_at_the_date_line, cell_count):
        assert cell_count == len(calc._create_grid(region_far_out_at_the_date_line, width, height, snap=snap)[0])

    @pytest.mark.parametrize(
        "width, height, snap, cell_count", [
            (0.125, 0.125, False, 2),
            (0.125, 0.125, True, 6)
        ])
    def test_create_grid_well_known(self, calc, width, height, snap, region_small_but_well_known, cell_count):
        assert cell_count == len(calc._create_grid(region_small_but_well_known, width, height, snap=snap)[0])

    @pytest.mark.parametrize(
        "width, height, include_center_cols, col_count", [
            (0.3, 0.3, True, 3),
            (0.3, 0.3, False, 1)
        ])
    def test_create_grid_center_cols(self, calc, width, height, include_center_cols, region_box_north_of_equator, col_count):
        grid, *_ = calc._create_grid(region_box_north_of_equator, width, height, include_center_cols=include_center_cols)
        assert col_count == len(grid.columns)

    @pytest.mark.parametrize(
        "length, first_heading, first_incomplete, last_incomplete, period", [
            (3, "2018 NOx emissions [kg] (incomplete)", True, True, "period_long"),  # 2018-10-15 to 2020-02-29
            (2, "2019 NOx emissions [kg]", False, True, "period_medium"),  # 2019-01-01 to 2020-01-31
            (2, "2019 NOx emissions [kg] (incomplete)", True, False, "period_medium_other"),  # 2019-11-01 to 2020-12-31
            (1, "2020 NOx emissions [kg] (incomplete)", True, True, "period_medium_third"),  # 2020-01-01 to 2020-11-15
            (1, "2020 NOx emissions [kg] (incomplete)", True, True, "period_short"),  # 2020-02-01 to 2020-02-29
            (1, "2020 NOx emissions [kg] (incomplete)", True, True, "period_short_other"),  # 2020-02-01 to 2020-02-20
            (1, "2018 NOx emissions [kg] (incomplete)", True, True, "period_very_short")  # 2018-10-15 to 2018-10-25
        ])
    def test_create_column_headings_per_year(self, calc, length, first_heading, first_incomplete, last_incomplete,
                                             period, request):
        period = request.getfixturevalue(period)
        column_headings = calc._create_column_headings_per_year(period, Pollutant.NOx)

        assert length == len(column_headings)
        assert first_heading == column_headings[0]
        assert first_incomplete == column_headings[0].endswith(" (incomplete)")
        assert last_incomplete == column_headings[-1].endswith(" (incomplete)")

    @pytest.mark.parametrize(
        "length, first_heading, first_incomplete, last_incomplete, period", [
            (17, "2018-10 NOx emissions [kg] (incomplete)", True, False, "period_long"),  # 2018-10-15 to 2020-02-29
            (13, "2019-01 NOx emissions [kg]", False, False, "period_medium"),  # 2019-01-01 to 2020-01-31
            (14, "2019-11 NOx emissions [kg]", False, False, "period_medium_other"),  # 2019-11-01 to 2020-12-31
            (11, "2020-01 NOx emissions [kg]", False, True, "period_medium_third"),  # 2020-01-01 to 2020-11-15
            (1, "2020-02 NOx emissions [kg]", False, False, "period_short"),  # 2020-02-01 to 2020-02-29
            (1, "2020-02 NOx emissions [kg] (incomplete)", True, True, "period_short_other"),  # 2020-02-01 to 2020-02-20
            (1, "2018-10 NOx emissions [kg] (incomplete)", True, True, "period_very_short")  # 2018-10-15 to 2018-10-25
        ])
    def test_create_column_headings_per_month(self, calc, length, first_heading, first_incomplete, last_incomplete,
                                              period, request):
        period = request.getfixturevalue(period)
        column_headings = calc._create_column_headings_per_month(period, Pollutant.NOx)

        assert length == len(column_headings)
        assert first_heading == column_headings[0]
        assert first_incomplete == column_headings[0].endswith(" (incomplete)")
        assert last_incomplete == column_headings[-1].endswith(" (incomplete)")


