import pytest
from shapely.geometry import shape

from eocalc.context import Pollutant
from eocalc.methods.base import TimeAggregation
from eocalc.methods.fluky import RandomEOEmissionCalculator


@pytest.fixture
def calc():
    return RandomEOEmissionCalculator()


@pytest.fixture
def region():
    return shape({"type": "Polygon",
                  "coordinates": [[[-122., 37.], [-125., 37.], [-125., 38.], [-122., 38.], [-122., 37.]]]})


class TestRandomMethods:

    def test_minimum_area(self, calc):
        assert 1 == calc.minimum_area_size()

    def test_covers(self, calc, region_sample_north, region_sample_south, region_sample_span_equator):
        assert calc.covers(region_sample_north)
        assert calc.covers(region_sample_south)
        assert calc.covers(region_sample_span_equator)

    def test_minimum_period(self, calc):
        assert 1 == calc.minimum_period_length()

    def test_supports(self, calc):
        for p in Pollutant:
            assert calc.supports(p)

        assert not calc.supports(None)

    @pytest.mark.parametrize("period", ["period_long", "period_medium", "period_short", "period_very_short"])
    def test_run(self, calc, region, period, request):
        period = request.getfixturevalue(period)

        for pollutant in Pollutant:
            table, grid = calc.run(region, period, TimeAggregation.YEARLY, pollutant)
            assert table is not None and grid is not None

        for granularity in TimeAggregation:
            table, grid = calc.run(region, period, granularity, Pollutant.NOx)
            assert table is not None and grid is not None

            column_count = len(grid.columns)
            if granularity is TimeAggregation.YEARLY:
                assert column_count == 6 + 1 + period.years
            elif granularity is TimeAggregation.MONTHLY:
                assert column_count == 6 + 1 + period.months
            else:
                assert column_count == 6 + 1 + period.days

        with pytest.raises(AttributeError):
            calc.run(region, period, granularity, None)
