"""Space emission calculator base classes and definitions."""

import math
from abc import ABC, abstractmethod
from collections import namedtuple
from datetime import date, timedelta
from enum import Enum, IntEnum, auto
from typing import Union

import numpy as np
from geopandas import GeoDataFrame
from pandas import DataFrame, Series
from pyproj import Transformer, CRS
from shapely.geometry import MultiPolygon, box
from shapely.ops import transform

from eocalc.context import Pollutant, GNFR


class Status(Enum):
    """Represent state of calculator."""

    READY = auto()
    RUNNING = auto()


class DateRange:
    """Represent a time span between two dates. Includes both start and end date.

    Attributes
    ----------
    start : date
        First day included in period.
    end : date
        Final day included in period.
    days : int
        Number of days covered by period, equal to len().
    months : int
        Number of months "touched" by this period.
        E.g. 2030-01-15 to 2030-04-30 returns 4
    years : int
        Number of years "touched" by this period.
        E.g. 2030-01-15 to 2042-12-31 returns 13
    """

    def __init__(self, start: Union[date, str], end: Union[date, str]):
        """
        Parameters
        ----------
        start : Union[date, str]
            First day to include in period. Parsed to date, if given as string (e.g. "2030-02-15").
        end : Union[date, str]
            Final day to include in period. Parsed to date, if given as string (e.g. "2042-10-01").
        """
        self.start = start
        self.end = end

        self.days = len(self)
        self.months = (self.end.year - self.start.year) * 12 + (self.end.month - self.start.month) + 1
        self.years = self.end.year - self.start.year + 1

    def __str__(self) -> str:
        return f"[{self.start} to {self.end}, {len(self)} days]"

    def __eq__(self, other: object) -> bool:
        return isinstance(other, type(self)) and (self.start, self.end) == (other.start, other.end)

    def __hash__(self) -> int:
        return hash((self.start, self.end))

    def __len__(self) -> int:
        return (self.end - self.start).days + 1

    def __iter__(self):
        yield from (self.start + timedelta(days=count) for count in range(len(self)))

    def __setattr__(self, key, value):
        if key in ["start", "end"] and not isinstance(value, date):
            super.__setattr__(self, key, date.fromisoformat(value))
        else:
            super.__setattr__(self, key, value)

        if hasattr(self, "start") and hasattr(self, "end") and self.end < self.start:
            raise ValueError(f"Invalid date range, end ({self.end}) cannot be before start ({self.start})!")


class TimeAggregation(IntEnum):
    """Time-wise granularity of method results."""

    DAILY = 0
    MONTHLY = 1
    YEARLY = 2


class EOEmissionCalculator(ABC):
    """Base class for all emission calculation methods to implement."""

    def __init__(self):
        super().__init__()

        self._state = Status.READY
        self._progress = 0

    @property
    def state(self) -> Status:
        """
        Check on the status of the calculator.

        Returns
        -------
        Status
            Current state of the calculation method.
        """
        return self._state

    @property
    def progress(self) -> int:
        """
        Check on the progress of the calculator after calling run().

        Returns
        -------
        int
            Progress in percent.
        """
        return self._progress

    @staticmethod
    @abstractmethod
    def minimum_area_size() -> int:
        """
        Check minimum region size this method can reliably work on.

        Returns
        -------
        int
            Minimum region size in km² (square kilometers).
        """

    @staticmethod
    @abstractmethod
    def coverage() -> MultiPolygon:
        """
        Get spatial extend the calculation method can cover.

        Returns
        -------
        MultiPolygon
            Representation of the area the method can be applied to.
        """

    @classmethod
    def covers(cls, region: MultiPolygon) -> bool:
        """
        Check for the calculation method's applicability to given region.

        Parameters
        ----------
        region : MultiPolygon
            Area to check.

        Returns
        -------
        bool
            If this method support emission estimation for given area.
        """
        return cls.coverage().contains(region)

    @staticmethod
    @abstractmethod
    def minimum_period_length() -> int:
        """
        Check minimum time span this method can reliably work on.

        Returns
        -------
        int
            Minimum period in number of days.
        """

    @staticmethod
    @abstractmethod
    def earliest_start_date() -> date:
        """
        Check if the method can be used for given period.

        Returns
        -------
        date
            Specific day the method becomes available.
        """

    @staticmethod
    @abstractmethod
    def latest_end_date() -> date:
        """
        Check if the method can be used for given period.

        Returns
        -------
        date
            Specific day the method becomes unavailable.
        """

    @staticmethod
    @abstractmethod
    def minimum_granularity() -> TimeAggregation:
        """
        Check time disaggregation method supports.

        Returns
        -------
        TimeAggregation
            Minimum granularity (day, month, year) the method can offer.
        """

    @staticmethod
    @abstractmethod
    def supports(pollutant: Pollutant) -> bool:
        """
        Check for the calculation method's applicability to given pollutant.

        Parameters
        ----------
        pollutant : Pollutant
            Pollutant to check.

        Returns
        -------
        bool
            If this method support estimation of given pollutant.
        """

    @abstractmethod
    def run(self, region: MultiPolygon, period: DateRange,
            granularity: TimeAggregation, pollutant: Pollutant) -> tuple[DataFrame, GeoDataFrame]:
        """
        Run method for given input and return the derived emission values.

        Results are given as a named tuple('totals', 'grid') where 'totals' is a GNFR DataFrame
        table with the regions per-sector and total emissions and 'grid' provides a GeoDataFrame
        containing the same results in gridded form. Detailed documentation is available at
        https://github.com/UBA-DE-Emissionsituation/space-emissions/wiki/Emission-deriving-methods

        Parameters
        ----------
        region : MultiPolygon
            Area to calculate emissions for.
            Needs to be covered by method and needs to satisfy the minimum area size requirement.
        period : DateRange
            Time span to cover.
            Methods will have limits on the periods they can calculate emissions for,
            e.g. a minimum period length and start/end dates.
        granularity : TimeAggregation
            Desired time resolution. Emission values will be aggregated to match granularity given.
        pollutant : Pollutant
            Air pollutant to calculate emissions for, needs to be supported.

        Returns
        -------
        totals : DataFrame
            GNFR table with the region's total and sectorized emission values and associated uncertainties.
        grid : GeoDataFrame
            Gridded emission results for the full area and period given.

        Raises
        ------
        ValueError
            If any of the parameters given does to meet the method's requirements.
        """

    def _validate(self, region: MultiPolygon, period: DateRange, granularity: TimeAggregation, pollutant: Pollutant):
        """Check inputs to run() method. Raise ValueError in case of a problem."""
        if not self.covers(region):
            raise ValueError("Region not covered by emission estimation method!")
        # EPSG:4326 is the shapely default (WGS84), EPSG:8857 is the Equal earth projection
        projection = Transformer.from_crs(CRS("EPSG:4326"), CRS("EPSG:8857"), always_xy=True).transform
        if transform(projection, region).area / 10 ** 6 < self.minimum_area_size():
            raise ValueError("Region too small!")

        if len(period) < self.minimum_period_length():
            raise ValueError(f"Time span {period} too short (minimum is {self.minimum_period_length()} days)!")
        if period.start < self.earliest_start_date():
            raise ValueError(f"Method cannot be used for period starting on {period.start}!")
        if period.end > self.latest_end_date():
            raise ValueError(f"Method cannot be used for period ending on {period.end}!")

        if granularity < self.minimum_granularity():
            raise ValueError(f"Method does not support {granularity.name} disaggregation!")

        if not self.supports(pollutant):
            raise ValueError(f"Pollutant {pollutant.name} not supported!")

    @staticmethod
    def _combine_uncertainties(values: Series, uncertainties: Series) -> float:
        """
        Calculate combined uncertainty using simple error propagation.

        Uses IPCC Guidelines formula 6.3 to aggregate and weight given values and uncertainties.
        U = sqrt((x1 * u1)^2 + ... + (xn * un)^2) / x1 + ... + xn

        Parameters
        ----------
        values : Series
            List of values to combine uncertainties for.
        uncertainties : Series
            List of uncertainties for values given.

        Returns
        -------
        float
            Combined uncertainty.
        """
        if sum(values.abs().dropna()) == 0:
            return 0
        elif len(values) > len(uncertainties):
            raise ValueError("List of uncertainties needs to have at least as many items as the list of values.")
        elif any(uncertainties.isnull()):
            raise ValueError("All uncertainties need to be numbers.")
        else:
            values, uncertainties = values.reset_index(drop=True), uncertainties.reset_index(drop=True)
            return (values.multiply(uncertainties, fill_value=0) ** 2).sum() ** 0.5 / values.abs().sum()

    @staticmethod
    def _create_gnfr_table(pollutant: Pollutant) -> DataFrame:
        """
        Generate empty result data frame.

        Has one row per GNFR sector, plus a row named "Totals". Also comes with three columns
        for the emission values and min/max uncertainties. All rows are pre-filled with n/a.

        Parameters
        ----------
        pollutant : Pollutant
            Pollutant name to include in first column name.

        Returns
        -------
        DataFrame
            Table to be filled by calculation methods.
        """
        cols = [f"{pollutant.name} emissions [kt]", "Umin [%]", "Umax [%]"]
        return DataFrame(index=list(GNFR), columns=cols, data=np.nan).append(
            DataFrame(index=["Totals"], columns=cols, data=np.nan))

    @staticmethod
    def _create_grid(region: MultiPolygon, width: float, height: float,
                     padding: tuple[float, float] = (0., 0.), snap: bool = False,
                     include_center_cols: bool = False, crs: str = "EPSG:4326") -> tuple[GeoDataFrame, tuple[int, int]]:
        """
        Overlay given region with grid data frame.

        Each cell will be created as a polygon and added to the frame, starting at the
        bottom left and then moving up row by row. Thus, the last item in the frame
        will represent the top right (north-east) corner cell of the grid.

        Parameters
        ----------
        region : MultiPolygon
            Area to cover.
        width : float
            Cell width [degrees].
        height : float
            Cell height [degrees].
        padding : tuple[float, float]
            Make grid cover area beyond the region given.
            Padding is given as two values >= 0 [degrees] (east/west, south/north). Defaults to no padding (0., 0.).
        snap : bool
            Make grid corners snap. If True, the lower left corner of the lower left cell
            will have long % width == 0 and lat % height == 0. If False, region bounds will
            be used. Defaults to False.
        include_center_cols : bool
            Add lat/long columns to data frame with cell center coordinates. Defaults to False.
        crs : str
            CRS to set on the data frame. Defaults to "EPSG:4326" (WGS84).

        Returns
        -------
        grid : GeoDataFrame
            Data frame with cell features spanning the full area. Will contain at least one cell (row).
        dimensions : tuple[int, int]
            Grid's cell counts (width, height).
        """
        grid = {"Center latitude [°]": [], "Center longitude [°]": [], "geometry": []}
        padded_bounds = (
            region.bounds[0] - padding[0],
            region.bounds[1] - padding[1],
            region.bounds[2] + padding[0],
            region.bounds[3] + padding[1]
        )

        min_long, min_lat, max_long, max_lat = padded_bounds if not snap else (
            padded_bounds[0] - padded_bounds[0] % width,
            padded_bounds[1] - padded_bounds[1] % height,
            padded_bounds[2] + (width - padded_bounds[2] % width if padded_bounds[2] % width != 0 else 0),
            padded_bounds[3] + (height - padded_bounds[3] % height if padded_bounds[3] % height != 0 else 0)
        )

        dimensions = namedtuple("Dimensions", ["long_cell_count", "lat_cell_count"])(
            # round() avoids cases like ceil(4.00000000000001) => 5, adding too many cells
            math.ceil(round((max_long - min_long) / width, 3)),
            math.ceil(round((max_lat - min_lat) / height, 3))
        )
        for lat in (min_lat + y * height for y in range(dimensions.lat_cell_count)):
            for long in (min_long + x * width for x in range(dimensions.long_cell_count)):
                grid["Center latitude [°]"] += [lat + height / 2]
                grid["Center longitude [°]"] += [long + width / 2]
                grid["geometry"] += [box(long, lat, long + width, lat + height)]

        return GeoDataFrame(grid if include_center_cols else {"geometry": grid["geometry"]}, crs=crs), dimensions

    @staticmethod
    def _create_column_headings_per_year(period: DateRange, pollutant: Pollutant) -> list[str]:
        """
        Prepare list of yearly column headings for gridded result frame.
        Takes into account partly covered years and labels those correctly.

        Parameters
        ----------
        period : DateRange
            Time period to cover.
        pollutant : Pollutant
            Pollutant name to include in column headings.

        Returns
        -------
        list[str]
            List of strings "YYYY PPP emissions [kg]" for the full period.
        """
        result = [f"{year} {pollutant.name} emissions [kg]"
                  for year in range(period.start.year, period.start.year + period.years)]

        if (period.start.day != 1 or period.start.month != 1) or \
                (period.years == 1 and (period.end.month != 12 or period.end.day != 31)):
            result[0] += " (incomplete)"
        if period.years > 1 and (period.end.month != 12 or period.end.day != 31):
            result[-1] += " (incomplete)"

        return result

    @staticmethod
    def _create_column_headings_per_month(period: DateRange, pollutant: Pollutant) -> list[str]:
        """
        Prepare list of monthly column headings for gridded result frame.
        Takes into account partly covered month and labels those correctly.

        Parameters
        ----------
        period : DateRange
            Time period to cover.
        pollutant : Pollutant
            Pollutant name to include in column headings.

        Returns
        -------
        list[str]
            List of strings "YYYY-MM PPP emissions [kg]" for the full period.
        """
        result = []
        for count in range(period.start.month, period.start.month + period.months):
            year = period.start.year + int((count - 1) / 12)
            month = int(count % 12) if int(count % 12) > 0 else 12
            result += [f"{year}-{month:02} {pollutant.name} emissions [kg]"]

        if period.start.day > 1 or (period.months == 1 and (period.end + timedelta(days=1)).day != 1):
            result[0] += " (incomplete)"
        if period.months > 1 and (period.end + timedelta(days=1)).day != 1:
            result[-1] += " (incomplete)"

        return result

    @staticmethod
    def _create_result_tuple(totals: DataFrame, grid: GeoDataFrame) -> tuple[DataFrame, GeoDataFrame]:
        """Helper to ease and unify creation of resulting named tuple."""
        return namedtuple("Emissions", ["totals", "grid"])(totals, grid)
